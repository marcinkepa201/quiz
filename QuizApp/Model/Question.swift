//
//  Question.swift
//  QuizApp
//
//  Created by Marcin Kępa on 09/04/2020.
//  Copyright © 2020 Marcin Kępa. All rights reserved.
//

import Foundation
struct Question {
    let text: String
    let answer: [String]
    let correctAnswer: String
    
    init(q:String, a:[String], correctAnswer:String) {
        self.text = q
        self.answer = a
        self.correctAnswer = correctAnswer
    }
}
